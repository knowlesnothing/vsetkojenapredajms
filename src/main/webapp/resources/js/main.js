/**
 * html5 does not validate checkbox group - this is to make sure at least one checkbox is checked
 */
function validate() 
{ 
    var fields = $("input[name='catCheckboxes']").serializeArray(); 
    if (fields.length === 0) 
    { 
       // alert('Please select category for item'); 
        
        $( function() {
            $( "#dialog-message" ).dialog({
              modal: true,
              buttons: {
                Ok: function() {
                  $( this ).dialog( "close" );
                }
              }
            });
          } );
        // cancel submit
        return false;
    } 
    else 
    { 
       // alert(fields.length + " items selected"); 
    }
}