package sk.mb.vsetkojenapredajms.dao;
import java.util.List;

import sk.mb.vsetkojenapredajms.model.Item;


/**
 * @author Miso2
 *
 */
public interface ItemDAO {
	
	/**
	 * Return list of randomly chosen items for display as featured on the index page
	 * @return List<Item>
	 */
	List<Item> getRandomItems();

	/**
	 * Return list of items from selected category category is identified by number in
	 * query string in request link - category?id
	 *
	 * @param categoryId
	 * @return
	 */
	List<Item> getAllItemsInCategory(long categoryId);
	
	/**
	 * Returns item for displaying detailed view
	 * 
	 * @param itemId
	 * @return Item
	 */
	Item getItemDetails(long itemId);
	
	/**
	 * Return list of items for logged-in user
	 * 
	 * @param userId
	 * @return List<Item>
	 */
	List<Item> getMyItems(long userId);
	
	/**
	 * Return all items for which logged-in user has placed bids
	 * 
	 * @param userId
	 * @return List<Item>
	 */
	List<Item> getItemsWithMyBid(long userId);
	
	/**
	 * transaction!!! TODO - check first, if something about this item was changed,
	 * if true, update all item's details in the database from the form THEN
	 * insert into category_has_item records for item category or categories,
	 * if item belongs to more categories
	 * 
	 * @param item
	 * @param categoryIds
	 */
	void saveEditedItem(Item item, String[] categoryIds);

	/**
	 * Delete item with passed in itemId
	 * 
	 * @param itemId
	 */
	boolean deleteItem(long itemId);
	
	/**
	 * transaction!!! - insert all item's details into database from the form THEN
	 * insert into category_has_item records for item category or categories,
	 * if item belongs to more categories
	 * 
	 * @param item
	 * @param categoryIds
	 * @return 
	 */
	long addItem(Item item, String[] categoryIds);
}
