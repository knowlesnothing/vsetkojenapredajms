package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import sk.mb.vsetkojenapredajms.dao.CategoryDAO;
import sk.mb.vsetkojenapredajms.daoimpl.CategoryRowMapper;
import sk.mb.vsetkojenapredajms.model.Category;

@Repository
public class CategoryDAOImpl implements CategoryDAO {
	
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<Category> getAllCategories() {
		
		String sql = "SELECT * FROM category";
		
		return namedParamJdbcTemplate.query(sql, new CategoryRowMapper());
	}

	@Override
	public Category getSelectedCategory(long categoryId) {
		SqlParameterSource params = new MapSqlParameterSource("categoryId", categoryId);
		
		String sql = "SELECT id, category_name FROM category WHERE id = :categoryId";
		
		return namedParamJdbcTemplate.queryForObject(sql, params, new CategoryRowMapper());
	}
	

	@Override
	public List<Category> getItemCategories(long itemId) {
		SqlParameterSource params = new MapSqlParameterSource("itemId", itemId);
		String sql = "SELECT category.id, category.category_name "
				+ "FROM category "
				+ "JOIN category_has_item "
				+ "ON category.id = category_has_item.category_id "
				+ "WHERE category_has_item.item_id = :itemId;";
		
		return  namedParamJdbcTemplate.query(sql, params, new CategoryRowMapper());
	}
}



















