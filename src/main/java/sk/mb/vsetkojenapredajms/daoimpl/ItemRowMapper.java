package sk.mb.vsetkojenapredajms.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sk.mb.vsetkojenapredajms.model.Item;

public class ItemRowMapper implements RowMapper<Item> {

	@Override
	public Item mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Item item = new Item();
		item.setId(resultSet.getLong("id"));
		item.setItemName(resultSet.getString("item_name"));
		String itemDescription = (resultSet.getString("item_description"));
		item.setItemDescription(itemDescription.replace("\n", "<br />"));
		item.setItemImage(resultSet.getString("item_image"));
		item.setDateAdded(resultSet.getString("date_added"));
		item.setVisible(resultSet.getByte("visible"));
		item.setMinimumBid(resultSet.getDouble("minimum_bid"));
		item.setUserId(resultSet.getLong("user_id"));
	
		return item;
	}



}
