package sk.mb.vsetkojenapredajms.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import sk.mb.vsetkojenapredajms.daoimpl.ItemDAOImpl;

@Configuration
@ComponentScan("sk.mb.vsetkojenapredajms")
@EnableWebMvc
@EnableTransactionManagement
@Import({SecurityConfig.class})
public class WebMvcAppConfig extends WebMvcConfigurerAdapter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WebMvcAppConfig.class);
	
	@Bean
	public DataSource dataSource() {
		final JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		dataSourceLookup.setResourceRef(true);
		return dataSourceLookup.getDataSource("jdbc/t8j8-resource");
	}
	
    @Bean
    public ItemDAOImpl itemDAO() {
        // configure and return a class having @Transactional methods
        return new ItemDAOImpl(dataSource());
    }

    @Bean
    public PlatformTransactionManager txManager() {
        return new DataSourceTransactionManager(dataSource());
    }
    
    @Bean
    public MultipartResolver multipartResolver() throws IOException {
    	return new StandardServletMultipartResolver();
    }
    
	@Bean
	public UrlBasedViewResolver urlBasedViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/jsp/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/*.css").addResourceLocations("/resources/css/");
		// this thinks that the root for image links mentioned in the css file is the css folder
		registry.addResourceHandler("/css/images/design/*").addResourceLocations("/resources/images/design/");
		registry.addResourceHandler("/css/images/*").addResourceLocations("/resources/images/");
		registry.addResourceHandler("/images/items/*").addResourceLocations("/resources/images/items/");
		registry.addResourceHandler("/js/*.js").addResourceLocations("/resources/js/");
	}
		
/*	@Bean
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping rmhm = new RequestMappingHandlerMapping();
		rmhm.setUseSuffixPatternMatch(false);
		rmhm.setUseTrailingSlashMatch(false);
		return rmhm;
	}*/

	
	
}
