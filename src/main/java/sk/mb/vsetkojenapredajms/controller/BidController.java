package sk.mb.vsetkojenapredajms.controller;

import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sk.mb.vsetkojenapredajms.dao.BidDAO;
import sk.mb.vsetkojenapredajms.dao.ItemDAO;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Item;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class BidController {
	private static final Logger LOGGER = LoggerFactory.getLogger(BidController.class);
	@Autowired
	private ItemDAO itemDAO;

	@Autowired
	private BidDAO bidDAO;

	@Autowired
	private ServletContext servletContext;

	public long getUserId() {
		User user = (User) servletContext.getAttribute("user");
		return user.getUserId();
	}

	@GetMapping("/my-placed-bids")
	public String showMyPlacedBids(Model model) {
		LOGGER.info("in showMyPlacedBids()");
		long userId = getUserId();
		List<Item> itemsWithMyBid = itemDAO.getItemsWithMyBid(userId);
		model.addAttribute("itemsWithMyBid", itemsWithMyBid);

		List<Bid> myMaxBidsForAllItems = bidDAO.getMyMaxBidsForAllItems(userId);
		model.addAttribute("myMaxBidsForAllItems", myMaxBidsForAllItems);
		return "bid/myPlacedBids";
	}

	@PostMapping("/bids")
	public String bidHandler(Bid bid,
							@RequestParam(name = "action") String action,
							Model model) {
		if (action != null) {
			switch (action) {
				
				case "placeBid":
					bidDAO.placeBid(bid);
					break;
	
				case "cancelPlacedBid":
					bidDAO.cancelPlacedBid(bid);
					break;
					
				case "acceptBid":
					bidDAO.acceptBid(bid);
					break;
					
				case "cancelAcceptedBid":
					bidDAO.cancelAcceptedBid(bid);
					break;
	
				default:
					return "404";
				}
		}
		return "redirect:/item-detail?item-id=" + bid.getItemId();
	}
}
