package sk.mb.vsetkojenapredajms.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sk.mb.vsetkojenapredajms.dao.BidDAO;
import sk.mb.vsetkojenapredajms.dao.CategoryDAO;
import sk.mb.vsetkojenapredajms.dao.ItemDAO;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Category;
import sk.mb.vsetkojenapredajms.model.Item;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class CategoryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryDAO categoryDAO;

	@Autowired
	private ItemDAO itemDAO;

	@Autowired
	private BidDAO bidDAO;

	@Autowired
	private ServletContext servletContext;
	
	@PostConstruct
	public void initializeCategories() {
		LOGGER.info("in CategoryController initializeCategories()");

		List<Category> categoryList = categoryDAO.getAllCategories();
		servletContext.setAttribute("categoryList", categoryList);
	}
	
	public long getUserId() {
		User user = (User) servletContext.getAttribute("user");
		return user.getUserId();
	}

	@RequestMapping("category")
	public String showSelectedCategory(@RequestParam("category-id") Long categoryId,
										Model model) {
		LOGGER.info("in showSelectedCategory()");
		
		Category selectedCategory = null;
		
		if(categoryId != null) {
			try {
				selectedCategory = categoryDAO.getSelectedCategory(categoryId);
				model.addAttribute("selectedCategory", selectedCategory);
			} catch (EmptyResultDataAccessException e) {
				LOGGER.error("Error in CategoryController, showSelectedCategory(), case (\"category\"): ", e);
			} 
		}
		if (selectedCategory != null) {
			
			long userId = getUserId();
			
			List<Item> itemList = itemDAO.getAllItemsInCategory(categoryId);
			model.addAttribute("itemList", itemList);
	
			List<Bid> myMaxBidsForItemsInCategory = bidDAO.getMyMaxBidsForItemsInCategory(userId, categoryId);
			model.addAttribute("myMaxBidsForItemsInCategory", myMaxBidsForItemsInCategory);
	
			List<Bid> maxBidsForMyItems = bidDAO.getMaxBidsForMyItems(userId);
			model.addAttribute("maxBidsForMyItems", maxBidsForMyItems);
	
			return "category/category";
		}
		return "404";
	}
}
