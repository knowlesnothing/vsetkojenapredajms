package sk.mb.vsetkojenapredajms.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Item {

	private long id;
	
	@NotBlank
	@Size(max = 45, min = 5, message="- Item name should be between 5 - 45 characters long.")
	private String itemName;
	
	@NotBlank
	@Size(max = 500, min = 10, message="- Item description should be between 10 - 500 characters long.")
	private String itemDescription;
	
	@NotBlank
	@Size(max = 45, min = 5, message="- Image name cannot be left empty.")
	private String itemImage;
	
	private String dateAdded;
	
	private int visible;
	
	private double minimumBid;
	
	
	private long userId;
	
	public Item() {

	}

	public Item(long id, String itemName, String itemDescription, String itemImage, int visible, double minimumBid) {
		this.id = id;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemImage = itemImage;
		this.visible = visible;
		this.minimumBid = minimumBid;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public double getMinimumBid() {
		return minimumBid;
	}

	public void setMinimumBid(double minimumBid) {
		this.minimumBid = minimumBid;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", itemName=" + itemName + ", itemDescription=" + itemDescription + ", itemImage="
				+ itemImage + ", dateAdded=" + dateAdded + ", visible=" + visible + ", minimumBid=" + minimumBid
				+ ", userId=" + userId + "]";
	}
	

	
}
