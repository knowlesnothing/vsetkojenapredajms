
package sk.mb.vsetkojenapredajms.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Category implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private short id;
    private String categoryName;

    public Category() {
    }

    public Category(String name) {
        this.categoryName = name;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String name) {
        this.categoryName = name;
    }
    
    public String toString() {
		return "category name: " + categoryName + ", category id: " + id;
    }

}
