package sk.mb.vsetkojenapredajms.model;

import java.sql.Date;

public class Bid {
	
	private long bidId;

	private double bidAmount;
	
	private long itemId;
	
	private long userId;
	
	private Date dateBidPlaced;
	
	private boolean bidAccepted;
	
	public Bid() {
		
	}
	
	public Bid(long itemId, long userId) {
		this.itemId = itemId;
		this.userId = userId;
	}
	
	public Bid(double bidAmount, long itemId, long userId) {
		this.bidAmount = bidAmount;
		this.itemId = itemId;
		this.userId = userId;
	}

	public long getBidId() {
		return bidId;
	}
	
	public void setBidId(long bidId) {
		this.bidId = bidId;
	}

	public double getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(double bidAmount) {
		this.bidAmount = bidAmount;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getDateBidPlaced() {
		return dateBidPlaced;
	}

	public void setDateBidPlaced(Date dateBidPlaced) {
		this.dateBidPlaced = dateBidPlaced;
	}

	public boolean getBidAccepted() {
		return bidAccepted;
	}

	public void setBidAccepted(boolean bidAccepted) {
		this.bidAccepted = bidAccepted;
	}

	@Override
	public String toString() {
		return "Bid [bidId=" + bidId + ", bidAmount=" + bidAmount + ", itemId=" + itemId + ", userId=" + userId
				+ ", dateBidPlaced=" + dateBidPlaced + ", bidAccepted=" + bidAccepted + "]";
	}
}
